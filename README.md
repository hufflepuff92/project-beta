# CarCar

Team:

* Person 1 - Which microservice?
* Person 2 - Which microservice?

## Design

## Service microservice

Explain your models and integration with the inventory
microservice, here.

## Sales microservice

Explain your models and integration with the inventory
microservice, here.


## Ubiquitous Language

    Inventory
        Models
            -Manufacturer
            -VehicleModel
            -Automobile
        Views
            -api_automobiles
            -api_automobile
            -api_manufacturers
            -api_manufacturer
            -api_vehicle_models
            -api_vehicle_model
        Encoders
            -ManufacturerEncoder
            -VehicleModelEncoder
            -AutomobileEncoder
        URL
            -path("automobiles/", api_automobiles, name="api_automobiles")
            -path("automobiles/<str:vin>/", api_automobile, name="api_automobile")
            -path("manufacturers/", api_manufacturers, name="api_manufacturers")
            -path("manufacturers/<int:pk>/", api_manufacturer, name="api_manufacturer")
            -path("models/", api_vehicle_models, name="api_vehicle_models")
            -path("models/<int:pk>/", api_vehicle_model, name="api_vehicle_model")

    
    Sales
        Models
            -SalesPerson
            -SaleRecord
            -Customer
            -AutomobileVO
        Views
            -api_create_sales_person
            -api_create_customer
            -api_customer_detail
            -api_new_sale
            -api_sales_list
            -api_sales_person_detail
        Encoders
            -SalesPersonEncoder
            -CustomerEncoder
            -SalesRecordEncooder
            -AutomobileVOEncoder
        URL
            -path("customer/", api_create_customer, name="api_create_customer")
            -path("customer/<str:id>/", api_customer_detail, name="api_customer_detail")
            -path("sale/new", api_new_sale, name="api_new_sale")
            -path("sales/", api_sales_list, name="api_sales_list")
            -path("employee/", api_create_sales_person, name="api_create_sales_person")
            -path("employee/<int:id>/", api_sales_person_detail, name="api_sales_person_detail")
    
    Service
        Models
            -Technician
            -ServiceAppointment
            -CustomerVO
            -AutomobileVO
        Views
            -api_technician
            -api_service_appointment
            -api_service_appointment_details
            -api_service_history
        Encoders
            -TechnicianEncoder
            -ServiceAppointmentEncoder
            -CustomerVOEncoder
            -AutomobileVOEncoder
        URL
            -path("technician/", api_technician, name="api_technician")
            -path("service/", api_service_appointment, name="api_service_appointment")
            -path("service/<str:id>", api_service_appointment_detail, name="api_service_appointment_detail")
            -path("service/history", api_service_history, name="api_service_history")
